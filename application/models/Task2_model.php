<?php

use PhpParser\Node\Expr\FuncCall;
use SebastianBergmann\CodeCoverage\Driver\Selector;

class Task2_model extends CI_Model
{

    public function simpan($data)
    {
        $this->db->insert('tasks', $data);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
    public function detail($id)
    {
        $this->db->select('*');
        $this->db->from('tasks');
        $this->db->where('id', $id);

        $get = $this->db->get();
        if ($get->num_rows() != 0) {
            return $get->row_object();
        } else {
            return [];
        }
    }
    public function hapus($id)
    {
        $this->db->delete('tasks', ['id' => $id]);
        return $this->db->affected_rows() ? true : false;
    }

    public function ubah($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tasks', $data);
        return $this->db->affected_rows() ? true : false;
    }

    public function list_data()
    {
        $this->db->select('*');
        $this->db->from('tasks');

        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return [];
        }
    }
}
