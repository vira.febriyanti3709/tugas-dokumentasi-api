<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Task_management2 extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('task2_model');
    }
    public function index_post()
    {
        $data = $this->post();

        $success = $this->task2_model->simpan($data);

        if ($success) {
            $this->response([
                'status' => true,
                'message' => 'Insert success',
                'data' => [$data]
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Insert failed',
                'data' => []
            ], 400);
        }
    }
    public function index_get()
    {
        $id = $this->get('id');

        $data = $this->task2_model->detail($id);
        if ($data) {
            $this->response([
                'status' => true,
                'message' => 'Detail success',
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'There is no task with id =' . $id,
            ], 400);
        }
    }
    public function index_delete()
    {
        $id = $this->get('id');

        if ($id) {
            $success = $this->task2_model->hapus($id);

            if ($success) {
                $this->response([
                    'status' => true,
                    'message' => 'Delete success',
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Delete failed'
                ], 400);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'id belum didefenisikan'
            ], 400);
        }
    }

    public function update_post()
    {
        $id = $this->get('id');
        $data = $this->post();

        $success = $this->task2_model->ubah($id, $data);
        $newData = $this->task2_model->detail($id);

        $this->response([
            'status' => true,
            'message' => 'Update success',
            'data' => $newData
        ], 200);
    }

    public function list_get()
    {
        $data = $this->task2_model->list_data();

        $this->response([
            'status' => true,
            'message' => 'List loaded',
            'data' => $data
        ], 200);
    }
}
